window.onload = function() {			
	jQuery("#edit-body").append('<div id="ace-target-1"></div>');
	
    var editor = ace.edit("ace-target-1");
    editor.setTheme("ace/theme/solarized_dark");
    editor.setShowPrintMargin(false);

	
	var textarea = jQuery("#edit-body-und-0-value");
	editor.getSession().setValue(textarea.val());			
			
    var HtmlMode = require("ace/mode/html").Mode;
    editor.getSession().setMode(new HtmlMode());

	jQuery(".node-form").submit(function(){
		textarea.val(editor.getSession().getValue());
	});
	
};